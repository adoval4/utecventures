from django.db import models

MEDIA_ROOT_STARTUPS_THUMBNAIL_IMAGE = 'media/startups/thumbnails/'
MEDIA_ROOT_STARTUPS_COVER_IMAGE = 'media/startups/covers/'
MEDIA_ROOT_STARTUPS_IN_TEXT_IMAGE = 'media/startups/in_text/'

MEDIA_ROOT_WORKER_IMAGE = 'media/people/'

MEDIA_ROOT_AWARDS_IMAGE = 'media/startups/awards/'
MEDIA_ROOT_SPONSORS_IMAGE = 'media/sponsors/'

MEDIA_ROOT_PAGES_COVER_IMAGE = 'media/covers/'

MEDIA_ROOT_PERKS_THUMBNAIL_IMAGE = 'media/perks/'
MEDIA_ROOT_BLOG_THUMBNAIL_IMAGE = 'media/blogs/'


class Batch(models.Model):
	
	name = models.CharField(max_length=250)

	def __unicode__(self):
		return u'%s'%(self.name)

class Worker(models.Model):

	name = models.CharField(max_length=150)

	position = models.CharField(max_length=150)

	twitter_url = models.URLField(max_length=250)
	linkedin_url = models.URLField(max_length=250)

	photo = models.ImageField(upload_to=MEDIA_ROOT_WORKER_IMAGE)

	class Meta:
		abstract = True

	def __unicode__(self):
		return u'%s (%s)'%(self.name, self.position)

class Founder(Worker):
	def __unicode__(self):
		try:
			startup = Startup.objects.get(founders__id=self.id) 
			return u'%s (%s at %s)'%(self.name, self.position, startup.name)
		except:
			return u'%s (%s at UNSET-STARTUP)'%(self.name, self.position)

class Award(models.Model):

	name = models.CharField(max_length=150)
	icon_image = models.ImageField(upload_to=MEDIA_ROOT_AWARDS_IMAGE)

	def __unicode__(self):
		return u'%s'%(self.name)

class Sponsor(models.Model):

	name = models.CharField(max_length=150)
	icon_image = models.ImageField(upload_to=MEDIA_ROOT_SPONSORS_IMAGE)

	def __unicode__(self):
		return u'%s'%(self.name)

# Create your models here.
class Startup(models.Model):

	name = models.CharField(max_length=150)

	batch = models.ForeignKey('Batch', related_name='utec_venture_batch')

	url = models.URLField(max_length=250)

	tweeter_pitch_EN = models.TextField(max_length=300)
	tweeter_pitch_ES = models.TextField(max_length=300)

	thumbnail_photo = models.ImageField(upload_to = MEDIA_ROOT_STARTUPS_THUMBNAIL_IMAGE)
	cover_image = models.ImageField(upload_to=MEDIA_ROOT_STARTUPS_COVER_IMAGE)
	in_text_image = models.ImageField(upload_to=MEDIA_ROOT_STARTUPS_IN_TEXT_IMAGE)

	paragraph_1_EN = models.TextField()
	paragraph_1_ES = models.TextField()

	quote_paragraph_EN = models.TextField()
	quote_paragraph_ES = models.TextField()

	paragraph_2_EN = models.TextField()
	paragraph_2_ES = models.TextField()

	founders = models.ManyToManyField('Founder', related_name='team_member')

	awards = models.ManyToManyField('Award', related_name='awards_received', blank=True)

	def __unicode__(self):
		return u'%s (%s)'%(self.name, self.batch.name)

class Blog(models.Model):
	url = models.URLField(max_length=250)

	name_EN = models.CharField(max_length=250)
	name_ES = models.CharField(max_length=250)
	
	first_paragraph_EN = models.TextField()
	first_paragraph_ES = models.TextField()

	thumbnail_photo = models.ImageField(upload_to = MEDIA_ROOT_BLOG_THUMBNAIL_IMAGE)

	datetime = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ['datetime']

	def __unicode__(self):
		return u'%d - %s'%(self.id, self.name_ES)

class Perk(models.Model):
	name_EN = models.CharField(max_length=150)
	name_ES = models.CharField(max_length=150)

	description_EN = models.TextField()
	description_ES = models.TextField()

	thumbnail_photo = models.ImageField(upload_to = MEDIA_ROOT_PERKS_THUMBNAIL_IMAGE)

	def __unicode__(self):
		return u'%s'%(self.name_ES)


class UVTeamMember(Worker):
	pass

class UVMentor(Worker):
	pass

class UVTechAdvisor(Worker):
	pass

PAGES = (
    ('I', 'Inicio'),
    ('P', 'Portafolio'),
    ('N', 'Nosotros'),
    ('A', 'Aplicacion'),
    ('B', 'Beneficios'),
)

class Page(models.Model):

	GET_PAGES_NAME_DICT = { key: name for key, name in PAGES }
	GET_PAGES_KEY_BY_NAME_DICT = { name: key for key, name in PAGES }

	name = models.CharField(max_length=1, choices=PAGES, unique=True)

	cover_header_EN = models.TextField(max_length=500)
	cover_header_ES = models.TextField(max_length=500)
	
	cover_image = models.ImageField(upload_to=MEDIA_ROOT_PAGES_COVER_IMAGE)

	def __unicode__(self):
		return u'%s'%(self.GET_PAGES_NAME_DICT[self.name])

class Application(models.Model):
	email = models.EmailField()
	idea = models.TextField()
	business = models.TextField()
	status = models.TextField()
	markets = models.TextField()
	tech_plan = models.TextField()
	founders_info = models.TextField()

	datetime = models.DateTimeField(auto_now_add=True)

	def to_email_message(self):
		email_message_content = {
			('De', self.email),
			('Idea/producto', self.idea),
			('Negocio', self.business),
			('Estado', self.status),
			('Mercado', self.markets),
			('Plan tecnologico', self.tech_plan),
			('Informacion de fundadores', self.founders_info)
		}
		
		email_message = '\n\n'
		for key, val in email_message_content:
			email_message += key+':\n'
			email_message += val+'\n\n'

		return email_message

	def __unicode__(self):
		return u'<%s>'%(self.email)






