# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0002_auto_20150611_0202'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Sponsors',
            new_name='Sponsor',
        ),
        migrations.RenameModel(
            old_name='UVMentors',
            new_name='UVMentor',
        ),
        migrations.RenameModel(
            old_name='UVTechAdvisors',
            new_name='UVTechAdvisor',
        ),
    ]
