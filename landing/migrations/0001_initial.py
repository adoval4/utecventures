# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('idea', models.TextField()),
                ('business', models.TextField()),
                ('status', models.TextField()),
                ('markets', models.TextField()),
                ('tech_plan', models.TextField()),
                ('founders_info', models.TextField()),
                ('datetime', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Award',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('icon_image', models.ImageField(upload_to=b'media/startups/awards/')),
            ],
        ),
        migrations.CreateModel(
            name='Batch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=250)),
                ('name_EN', models.URLField(max_length=250)),
                ('name_ES', models.URLField(max_length=250)),
                ('first_paragraph_EN', models.TextField()),
                ('first_paragraph_ES', models.TextField()),
                ('thumbnail_photo', models.ImageField(upload_to=b'media/blogs/')),
                ('datetime', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['datetime'],
            },
        ),
        migrations.CreateModel(
            name='Founder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('position', models.CharField(max_length=150)),
                ('twitter_url', models.URLField(max_length=250)),
                ('linkedin_url', models.URLField(max_length=250)),
                ('photo', models.ImageField(upload_to=b'media/people/')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=1, choices=[(b'I', b'Inicio'), (b'P', b'Portafolio'), (b'N', b'Nosotros'), (b'A', b'Aplicacion')])),
                ('cover_header_EN', models.TextField(max_length=500)),
                ('cover_header_ES', models.TextField(max_length=500)),
                ('cover_image', models.ImageField(upload_to=b'media/covers/')),
            ],
        ),
        migrations.CreateModel(
            name='Perk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_EN', models.CharField(max_length=150)),
                ('name_ES', models.CharField(max_length=150)),
                ('description_EN', models.TextField()),
                ('description_ES', models.TextField()),
                ('thumbnail_photo', models.ImageField(upload_to=b'media/perks/')),
            ],
        ),
        migrations.CreateModel(
            name='Sponsors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('icon_image', models.ImageField(upload_to=b'media/sponsors/')),
            ],
        ),
        migrations.CreateModel(
            name='Startup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('url', models.URLField(max_length=250)),
                ('tweeter_pitch_EN', models.TextField(max_length=300)),
                ('tweeter_pitch_ES', models.TextField(max_length=300)),
                ('thumbnail_photo', models.ImageField(upload_to=b'media/startups/thumbnails/')),
                ('cover_image', models.ImageField(upload_to=b'media/startups/covers/')),
                ('in_text_image', models.ImageField(upload_to=b'media/startups/in_text/')),
                ('paragraph_1_EN', models.TextField()),
                ('paragraph_1_ES', models.TextField()),
                ('quote_paragraph_EN', models.TextField()),
                ('quote_paragraph_ES', models.TextField()),
                ('paragraph_2_EN', models.TextField()),
                ('paragraph_2_ES', models.TextField()),
                ('awards', models.ManyToManyField(related_name='awards_received', to='landing.Award')),
                ('batch', models.ForeignKey(related_name='utec_venture_batch', to='landing.Batch')),
                ('founders', models.ManyToManyField(related_name='team_member', to='landing.Founder')),
            ],
        ),
        migrations.CreateModel(
            name='UVMentors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('position', models.CharField(max_length=150)),
                ('twitter_url', models.URLField(max_length=250)),
                ('linkedin_url', models.URLField(max_length=250)),
                ('photo', models.ImageField(upload_to=b'media/people/')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UVTeamMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('position', models.CharField(max_length=150)),
                ('twitter_url', models.URLField(max_length=250)),
                ('linkedin_url', models.URLField(max_length=250)),
                ('photo', models.ImageField(upload_to=b'media/people/')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UVTechAdvisors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('position', models.CharField(max_length=150)),
                ('twitter_url', models.URLField(max_length=250)),
                ('linkedin_url', models.URLField(max_length=250)),
                ('photo', models.ImageField(upload_to=b'media/people/')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
