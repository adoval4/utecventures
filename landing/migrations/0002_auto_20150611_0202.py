# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='name_EN',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='blog',
            name='name_ES',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='startup',
            name='awards',
            field=models.ManyToManyField(related_name='awards_received', to='landing.Award', blank=True),
        ),
    ]
