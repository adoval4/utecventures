from django.shortcuts import render
from django.core.mail import send_mail
from django import forms
from django.utils import translation

from models import *

# values related to languages election
LANG_KEY = 'lang'
SPANISH_LANG = 'es'
ENGLISH_LANG = 'en'
DEFAULT_LANG = ENGLISH_LANG

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def set_session_langague(request):
	change = False
	if LANG_KEY in request.GET:
		language = request.GET[LANG_KEY]
		change = True
	else:
		language = translation.get_language_from_request(request)
		print 'Language detected=%s'%(language,)
	if LANG_KEY not in request.session or change:
		if language == SPANISH_LANG:
			request.session[LANG_KEY] = SPANISH_LANG
		elif language == ENGLISH_LANG:
			request.session[LANG_KEY] = ENGLISH_LANG
		else:
			request.session[LANG_KEY] = DEFAULT_LANG
		print 'Language to use=%s'%(request.session[LANG_KEY],)
	return request

# Create your views here.
def index(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		page = Page.objects.get(name=Page.GET_PAGES_KEY_BY_NAME_DICT['Inicio'])
		portafolio = Startup.objects.all()
		blog = Blog.objects.all()
		sponsors = Sponsor.objects.all()
		context_data = {
			'base_url': '//'+request.get_host(),			
			'page': page, 
			'portafolio': portafolio, 
			'blog': blog, 
			'sponsors': sponsors,
			'request': request
		}
		return render(request, 'index.html', context_data)

def portafolio(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		page = Page.objects.get(name=Page.GET_PAGES_KEY_BY_NAME_DICT['Portafolio'])
		portafolio = Startup.objects.all()
		context_data = {
			'base_url': '//'+request.get_host(),
			'page': page, 
			'portafolio': portafolio, 
			'request': request
		}
		return render(request, 'portafolio_site.html', context_data)

def startup(request, startup_id):
	request = set_session_langague(request)
	if request.method == 'GET':
		startup = Startup.objects.get(id=startup_id)
		context_data = {
			'base_url': '//'+request.get_host(),			
			'startup': startup, 
			'request': request
		}
		return render(request, 'portafolio-perfilstartup-equipo_site.html', context_data)

def perks(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		perks = Perk.objects.all()
		page = Page.objects.get(name=Page.GET_PAGES_KEY_BY_NAME_DICT['Beneficios'])
		context_data = {
			'base_url': '//'+request.get_host(),
			'page': page, 
			'perks': perks, 
			'request': request
		}
		return render(request, 'beneficios_site.html', context_data)

def blog(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		blog = Blog.objects.all()
		context_data = {
			'base_url': '//'+request.get_host(),
			'blog': blog, 
			'request': request
		}
		return render(request, 'blogutec_ventures_site_mobile.html', context_data)

def about_us(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		team_members = UVTeamMember.objects.all()
		mentors = UVMentor.objects.all()
		tech_advisors = UVTechAdvisor.objects.all()
		page = Page.objects.get(name=Page.GET_PAGES_KEY_BY_NAME_DICT['Nosotros'])
		context_data = {
			'base_url': '//'+request.get_host(),
			'team_members': team_members, 
			'mentors': mentors, 
			'tech_advisors': tech_advisors, 
			'page': page, 
			'request': request
		}
		return render(request, 'nosotros_site.html', context_data)

class ApplicationForm(forms.Form):
	email = forms.EmailField()
	idea = forms.CharField()
	business = forms.CharField()
	status = forms.CharField()
	markets = forms.CharField()
	tech_plan = forms.CharField()
	founders_info = forms.CharField()

def apply(request):
	request = set_session_langague(request)
	if request.method == 'GET':
		page = Page.objects.get(name=Page.GET_PAGES_KEY_BY_NAME_DICT['Nosotros'])

		context_data = {
			'base_url': '//'+request.get_host(),
			'page': page, 
			'request': request
		}
		return render(request, 'postulacion_site.html', context_data)
	elif request.method == 'POST':
		form = ApplicationForm(request.POST)
		if form.is_valid():
			email = forms.EmailField()
			idea = forms.CharField()
			business = forms.CharField()
			status = forms.CharField()
			markets = forms.CharField()
			tech_plan = forms.CharField()
			founders_info = forms.CharField()

			try:
				new_application = Application.objects.create(
					email = email,
					idea = idea,
					business = business,
					status = status,
					markets = markets,
					tech_plan = tech_plan,
					founders_info = founders_info
				)
				APPLICATION_ALERT_EMAIL = 'ventures@utec.edu.pe'

				email_message = new_application.to_email_message()
				send_mail(
					'UTEC Ventures: Nueva aplicacion', 
					email_message, 
					'app@utecventures.com',
				    [APPLICATION_ALERT_EMAIL], 
				    fail_silently=False
				)

				new_application.save()

				return HttpResponse('OK')
			except IntegrityError:
				return HttpResponse('REPEATED')

		else: 
			data = {'errors': [error for error in form.errors]}
			return HttpResponse(json.dumps(data, sort_keys=True, indent=4), content_type="application/json")


