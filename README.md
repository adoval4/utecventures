REQUERIMIENTOS:
===============

Corre en un servidor Ubuntu 13.04 LTS de 256mb RAM and 20GB.

Sigan estas intrucciones:
-------------------------
https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-django-with-postgres-nginx-and-gunicorn

SERVIDOR NGINX
--------------
Las configuraciones del servidor estatico nginx se encuentran en NGINX_config.

BASE DE DATOS
-------------
Crear base de datos llamada utecventdb.
Colocar usuario de postgres y password en utecventures/settings.py.



